﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_blog.Data;
using back_blog.JsonModels;
using back_blog.Models;
using back_blog.Utils;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace back_blog.Controllers
{
    [Route(ParamentrosProjeto.VersionAPi)]
    public class UsersController : MyController
    {

        public UsersController(BloggingContext context) : base(context)
        {
     
        }

        [HttpGet]
        [Route("users")]
        public IActionResult Index()
        {
            //1 forma
            //var model = new UserJsonModel();

            //model.CarregaDados(this);

            //2 forma

            var model = db.Users.ToList();

            return Json(new { ok = model });
        }

        [HttpPost]
        [Route("users/create")]
        public IActionResult Post([FromBody] UserJsonModel modelJson)
        {

            modelJson.TrataDados(this);

            if (ModelState.IsValid)
            {

                using (var transaction = db.Database.BeginTransaction())
                {

                    try
                    {
                        var passwordHasher = new PasswordHasher<User>();
                        var user = new User();
                        var hashedPassword = passwordHasher.HashPassword(user, modelJson.SenhaUsuario);


                        User tbUsers = new User()
                        {
                            Name = modelJson.NomeUsuario,
                            Passoword = hashedPassword,
                        };
                        db.Users.Add(tbUsers);
                        db.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        return Json(new { erro = "Erro ao salvar no banco de dados" + ex.Message });
                    }



                    
                }

                return Json(new { ok = "Usuario criado com sucesso" });
            }


            var listErroValidation = ModelState.Values.SelectMany(a => a.Errors.Select(e => e.ErrorMessage));
            return Json(new { erro = listErroValidation });
        }



    }
}
