﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_blog.Data;
using Microsoft.AspNetCore.Mvc;

namespace back_blog.Controllers
{
    public class MyController : Controller
    {
        //private readonly BloggingContext _context;

        protected readonly BloggingContext db;

        public BloggingContext Db => db;


        public MyController(BloggingContext context)
        {
            this.db = context;
        }


        public IActionResult Base()
        {
            return Ok("ok");
        }
    }
}
