﻿using back_blog.Controllers;
using back_blog.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace back_blog.JsonModels
{
    public class UserJsonModel
    {
        [Required(ErrorMessage = "Nome obrigatório")]
        public string NomeUsuario { get; set; }

        [Required(ErrorMessage = "Senha obrigatório")]
        public string SenhaUsuario { get; set; }

        [JsonIgnore]
        public bool ExistUser { get; set; }

        //[JsonIgnore]
        public List<User> ListagemsUsers { get; set; }

        public JsonResult CarregaDados(MyController myG)
        {

            ListagemsUsers = myG.Db.Users.ToList();

            return null;
        }


        public JsonResult ExisteUser(string nome, MyController BaseDados)
        {

            var user = BaseDados.Db.Users.Where(us => us.Name == nome);

            if (user != null)
            {
                ExistUser = true;
            }

            return null;
        }


        public JsonResult TrataDados(MyController myG)
        {

            if (string.IsNullOrEmpty(NomeUsuario) || string.IsNullOrEmpty(SenhaUsuario))
            {
                myG.ModelState.AddModelError("Paramento", "Nome ou senha estao vazios");
            }

            return null;
        }

    }
}
